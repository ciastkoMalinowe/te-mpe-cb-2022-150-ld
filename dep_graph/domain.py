"""Contains the domain objects used by the dependency graph resolution."""
from dataclasses import dataclass
from typing import Tuple

@dataclass(frozen=True)
class Package:
    """Represents a package with the dependencies used as a single node in the DependencyGraph.
    Please note that the class assumes no circular dependencies,
    if one creates object manually circular dependencies will lead to the StackOverflow in __eq__.
    The order of the packages matters (packages with the same dependencies but in a different order
    are not consider equal)

    Args:
        name: name of the package
        dependencies: list of the packages the package depend on
    """
    name: str
    dependencies: Tuple['Package']


@dataclass(frozen=True)
class DependencyGraph:
    """Represents a dependency graph of all packages.
    Please note that the class assumes no circular dependencies,
    if one creates object manually circular dependencies will lead to the StackOverflow in __eq__
    The order of the packages matters (graphs with the same dependencies but in a different order
    are not consider equal)

    Args:
        packages: list of packages
    """
    packages: Tuple[Package, ...]


class CyclicDependencyException(Exception):
    """Circular dependency detected."""


class PackageNotFoundException(Exception):
    """Package name is not in the keys."""


class GraphReadingException(Exception):
    """Failed to read the input graph from file."""
