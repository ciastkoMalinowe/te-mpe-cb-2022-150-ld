""" A module resolving a dependency tree from a JSON file from _FIXED_PATH."""

from dep_graph.logic import resolve_dependency_graph
