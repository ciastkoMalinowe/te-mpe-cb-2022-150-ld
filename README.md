# TE-MPE-CB-2022-150-LD

Contains solution for the recruitment exercise - a package `dep_graph` that reads a JSON file from a fixed filesystem location, traverses the dependencies and prints the resolved dependency graph to the standard output.

## Disclaimers

### Code
- I assumed that the graph with a cyclic dependency is invalid (like in python). Hence, in that case, an exception is thrown. (But It would be straighforward to modify the function to return all the cycles - ie by adding the stack with the visited nodes)
- I believe that using a Dict to represent a graph is a perfectly valid solution, but I used the datacalsses to make my code more readabl.
- I assumed that the order of the dependencis for the package matters. (If not, the frozen sets can be used instead of the tuples)
- Any duplicates in the dependencies list are removed. Duplicates in the JSON keys are handled by  the `json.load` used to load the JSON from the file.


### Good practices

Due to the limited time, I did not apply all the good practices I would normally put in place in the bigger project. Normally, I would: 

- work with branches (and run tests before commiting, unfortunately I needed to finish the excersise using the WebIDE, hence a lot of 'unnecessary' commits fixing the typos, imports etc)
- use yapf with pre-commit hook for formatting
- add stages deploying the package to the registry (ie PyPi), building the docs (sphinx) add hosting the docs (ie on GitLab Pages)
- configure and add logging

Depending on the users of the package and the execution environment, it might be good to test the code with different versions of python
