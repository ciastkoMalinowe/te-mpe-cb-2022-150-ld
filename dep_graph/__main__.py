"""Resolves dependency graph from the JSON saved in the provided location 
(_FIXED_PATH if not provided).
"""
import argparse
import dep_graph
from dep_graph import logic

_FIXED_PATH = '/tmp/deps.json'


def _get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('filesystem_location', default=_FIXED_PATH)
    return parser


def _main():
    parser = _get_parser()
    args = parser.parse_args()
    graph = dep_graph.resolve_dependency_graph(args.filesystem_location)
    print(logic.to_string_representation(graph))


if __name__ == '__main__':
    _main()
