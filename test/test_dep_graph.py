import dep_graph
import pytest
from os import path
from dep_graph import domain

_INPUT_JSONS_DIR = 'input_jsons'
_FILES_DIRECTORY = path.join(path.dirname(path.realpath(__file__)), _INPUT_JSONS_DIR)


def test_resolve_dependency_graph_example():
    expected_graph = _get_expected_for_example()
    returned_graph = dep_graph.resolve_dependency_graph(f'{_FILES_DIRECTORY}/example.json')
    assert expected_graph == returned_graph


def test_resolve_dependency_graph_not_existing():
    with pytest.raises(domain.GraphReadingException):
        dep_graph.resolve_dependency_graph(f'{_FILES_DIRECTORY}/not_existing_file.json')


def test_resolve_dependency_graph_invalid_json():
    with pytest.raises(domain.GraphReadingException):
        dep_graph.resolve_dependency_graph(f'{_FILES_DIRECTORY}/invalid.json')


def test_resolve_dependency_graph_repeated_keys():
    expected_graph = _get_expected_for_repeated()
    returned_graph = dep_graph.resolve_dependency_graph(f'{_FILES_DIRECTORY}/repeated_keys.json')
    assert expected_graph == returned_graph


def _get_expected_for_example():
    pkg3 = domain.Package('pkg3', ())
    pkg2 = domain.Package('pkg2', (pkg3,))
    pkg1 = domain.Package('pkg1', (pkg2, pkg3))
    return domain.DependencyGraph((pkg1, pkg2, pkg3))


def _get_expected_for_repeated():
    pkg3 = domain.Package('pkg3', ())
    pkg2 = domain.Package('pkg2', (pkg3,))
    pkg1 = domain.Package('pkg1', (pkg2,))
    return domain.DependencyGraph((pkg1, pkg2, pkg3))
