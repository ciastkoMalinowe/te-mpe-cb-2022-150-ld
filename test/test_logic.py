from os import path

import pytest

from dep_graph import logic, domain


_INPUT_JSONS_DIR = 'input_jsons'
_FILES_DIRECTORY = path.join(path.dirname(path.realpath(__file__)), _INPUT_JSONS_DIR)

def test_resolve_graph_from_dict_example():
    input_json = {
        'pkg1': ['pkg2', 'pkg3'],
        'pkg2': ['pkg3'],
        'pkg3': []
    }
    expected_graph = _get_expected_for_example()
    returned_graph = logic.resolve_graph_from_dict(input_json)
    assert expected_graph == returned_graph


def test_resolve_graph_empty():
    assert logic.resolve_graph_from_dict({}) == domain.DependencyGraph(())


def test_resolve_graph_from_dict_packages_repeated():
    input_json = {
        'pkg1': ['pkg2', 'pkg3', 'pkg2', 'pkg3', 'pkg2'],
        'pkg2': ['pkg3'],
        'pkg3': []
    }
    expected_graph = _get_expected_for_example()
    returned_graph = logic.resolve_graph_from_dict(input_json)
    assert expected_graph == returned_graph


def test_resolve_graph_from_dict_cyclic():
    input_json = {
        'pkg1': ['pkg2', 'pkg3'],
        'pkg2': ['pkg3'],
        'pkg3': ['pkg1']
    }
    with pytest.raises(domain.CyclicDependencyException):
        logic.resolve_graph_from_dict(input_json)


def test_resolve_graph_from_dict_cyclic_self():
    input_json = {
        'pkg1': ['pkg1', 'pkg3'],
        'pkg2': ['pkg3'],
        'pkg3': []
    }
    with pytest.raises(domain.CyclicDependencyException):
        logic.resolve_graph_from_dict(input_json)


def test_resolve_graph_from_dict_missing_node():
    input_json = {
        'pkg1': ['pkg2', 'pkg3'],
        'pkg2': ['pkg3', 'pkg4'],
        'pkg3': []
    }
    with pytest.raises(domain.PackageNotFoundException):
        logic.resolve_graph_from_dict(input_json)

def test_to_string_representation():
    input_json = _get_expected_for_example()

    result_string = logic.to_string_representation(input_json)
    expected_string = ' - pkg1\n   - pkg2\n     - pkg3\n   - pkg3\n - pkg2\n   - pkg3\n - pkg3'
    assert result_string == expected_string

def _get_expected_for_example():
    pkg3 = domain.Package('pkg3', ())
    pkg2 = domain.Package('pkg2', (pkg3,))
    pkg1 = domain.Package('pkg1', (pkg2, pkg3))
    return domain.DependencyGraph((pkg1, pkg2, pkg3))
