"""Contains the logic of the dependency graph resolution."""
import json
from typing import List, Dict, Set, Tuple
from dep_graph.domain import DependencyGraph, Package, \
GraphReadingException, PackageNotFoundException, CyclicDependencyException


def resolve_dependency_graph(filepath: str) -> DependencyGraph:
    """ Creates a dependency graph based on the node graph representation read from
    the provided file (JSON format).
    Uses depth first graph traversal (DFS) to detect circular dependencies.

    Args:
        filepath: a path to the file with the JSON representation of the graph
    Returns:
        a valid dependency graph (understood as an acyclic, directed graph)
    Raises:
        CyclicDependencyException if cycle detected
        PackageNotFoundException if package not found in the keys
        GraphReadingException if failed to read a JSON from a file
    """
    try:
        with open(filepath, encoding='utf-8') as file:
            packages = json.load(file)
    except Exception as exception:
        raise GraphReadingException from exception
    return resolve_graph_from_dict(packages)


def resolve_graph_from_dict(packages: Dict[str, List[str]]) -> DependencyGraph:
    """Resolves dependency graph from a dict (Using DFS). Visible for testing."""
    def resolve_package(name):
        if name not in packages:
            raise PackageNotFoundException()
        dependencies = packages[name]
        dependencies = _remove_duplicates(dependencies)  # log if duplicated packages detected
        not_yet_resolved = [dep for dep in dependencies if dep not in resolved]
        if len(not_yet_resolved) == 0:
            resolved[name] = Package(name, tuple(resolved[dep] for dep in dependencies))
        elif name in visited:
            raise CyclicDependencyException()
        else:
            visited.add(name)
            queue.append(name)
            queue.extend(reversed(not_yet_resolved))

    queue: List[str] = list(packages.keys())
    visited: Set[str] = set()
    resolved: Dict[str, Package] = {}

    while len(queue) > 0:
        package_name = queue.pop()
        if package_name not in resolved:
            resolve_package(package_name)

    return DependencyGraph(tuple(resolved[key] for key in reversed(resolved.keys())))


def to_string_representation(dependency_graph: DependencyGraph) -> str:
    """Makes a string representation of the DependencyGraph. Uses DFS. Visible for testing."""
    return '\n'.join('\n'.join(_to_tab_dash_tree(package)) for package in dependency_graph.packages)


def _to_tab_dash_tree(package: Package) -> List[str]:

    def render(pair: Tuple[Package, int]) -> str:
        return f'{"  " * pair[1]} - {pair[0].name}'

    stack: List[Tuple[Package, int]] = [(package, 0)]
    lines: List[str] = []

    while len(stack) > 0:
        package_pair = stack.pop()
        lines.append(render(package_pair))
        level = package_pair[1]
        stack.extend([(p, level+1) for p in reversed(package_pair[0].dependencies)])

    return lines


def _remove_duplicates(strings: List[str]) -> List[str]:
    deduplicated = {item:item for item in strings}  # preserves the order
    return list(deduplicated.keys())
